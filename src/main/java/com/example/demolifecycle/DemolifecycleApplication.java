package com.example.demolifecycle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemolifecycleApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemolifecycleApplication.class, args);
    }

}
